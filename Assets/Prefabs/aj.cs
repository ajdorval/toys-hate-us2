using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aj : MonoBehaviour
{
    Animator anim;
    public GameObject sam;
    Transform aj2;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        aj2 = GetComponent<Transform>();
        anim.enabled = false;
    }

    float getDistance()
    {
        Vector3 playerPos = sam.transform.position;
        float xDist = (aj2.position.x - playerPos.x);
        float zDist = (aj2.position.z - playerPos.z);
        float xSquare = Convert.ToSingle(Math.Pow(xDist, 2));
        float zSquare = Convert.ToSingle(Math.Pow(zDist, 2));
        return Convert.ToSingle(Math.Sqrt(xSquare + zSquare));
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float distance = this.getDistance();
        if (distance <= 5)
        {
            anim.enabled = true;
        }
    }
}
