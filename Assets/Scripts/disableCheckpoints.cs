using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableCheckpoints : MonoBehaviour
{
    public GameObject arcade;
    public GameObject toystore;
    public GameObject playarea;
    public GameObject dollstore;

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Player")
        {
            arcade.SetActive(false);
            toystore.SetActive(false);
            playarea.SetActive(false);
            dollstore.SetActive(false);
        }
    }
    
}
