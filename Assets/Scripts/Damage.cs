using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Damage : MonoBehaviour
{
    // Start is called before the first frame update
    public int damageNumber;
    public int damageMax = 11;
    public Image damageUI1;
    public Image damageUI2;
    public Image damageUI3;
    public Image damageUI4;
    public Text RText;
    private bool lock1;
    public CanvasGroup deathScreen;
    public CharacterController controller;
    public GameObject flashlight;
    public bool firstBattery;
    void Start()
    {
        damageNumber = 0;
        damageUI1.gameObject.SetActive(false);
        damageUI2.gameObject.SetActive(false);
        damageUI3.gameObject.SetActive(false);
        damageUI4.gameObject.SetActive(false);
        deathScreen.gameObject.SetActive(false);
        UnityEngine.Cursor.visible = false;

    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<Collider>().tag == "Battery") //if colliding with battery, add a battery
        {
            if (firstBattery == false)
            {
                firstBattery = true;
                StartCoroutine("refillUI");
            }
            Debug.Log("battery collision!");
            flashlight.GetComponent<FlashlightControl>().batteryIncrease(col);
        }

        if (col.GetComponent<Collider>().tag == "Sibling")
        {
            Debug.Log("you got the kid!");
            SceneManager.LoadScene("Win");
        }


        if (col.GetComponent<Collider>().tag == "Enemy") //if colliding with enemy, do damage
        {
            Debug.Log("an enemy...");
            if (damageNumber == 0)
            {
                damageNumber += 10;
                Debug.Log(damageNumber);
            }
            else
            {
                damageNumber += 7;
            }
        }
        if (damageNumber > damageMax)
        {
            PlayerDeath();
        }
        else if (damageNumber > 0 && lock1 == false)
        {
            lock1 = true;
            StartCoroutine("damageCountdown");
        }
    }
    public void PlayerDeath()
    {
        //UnityEngine.Cursor.visible = true;
        SceneManager.LoadScene("Death");
    }
    public IEnumerator damageCountdown()
    {
        while (damageNumber > 0)
        {
            damageNumber--;
            Debug.Log(damageNumber);
            if (damageNumber >= 9)
            {
                damageUI1.gameObject.SetActive(true);
                damageUI2.gameObject.SetActive(true);
                damageUI3.gameObject.SetActive(true);
                damageUI4.gameObject.SetActive(true);
            }
            if (damageNumber == 7)
            {
                damageUI1.gameObject.SetActive(true);
                damageUI2.gameObject.SetActive(true);
                damageUI3.gameObject.SetActive(true);
                damageUI4.gameObject.SetActive(false);
            }
            if (damageNumber == 4)
            {
                damageUI1.gameObject.SetActive(true);
                damageUI2.gameObject.SetActive(true);
                damageUI3.gameObject.SetActive(false);
                damageUI4.gameObject.SetActive(false);
            }
            if (damageNumber == 1)
            {
                damageUI1.gameObject.SetActive(true);
                damageUI2.gameObject.SetActive(false);
                damageUI3.gameObject.SetActive(false);
                damageUI4.gameObject.SetActive(false);
            }
            yield return new WaitForSeconds(1);
        }
        if (damageNumber == 0)
        {
            lock1 = false;
            damageUI1.gameObject.SetActive(false);
            damageUI2.gameObject.SetActive(false);
            damageUI3.gameObject.SetActive(false);
            damageUI4.gameObject.SetActive(false);
        }
    }
    public IEnumerator refillUI()
    {
        RText.gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        RText.gameObject.SetActive(false);
    }

}