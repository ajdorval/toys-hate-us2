using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    //to disable checkpoints from start perhaps?
    public GameObject arcade;
    public GameObject toystore;
    public GameObject playarea;
    public GameObject dollstore;
    public GameObject construction;
    //character movement stuff
    CharacterController characterController;
    public float walkingSpeed = 7.0f;
    public float runningSpeed = 13.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    public float rotMult = 4f;

    float yaw = 0f;
    float pitch = 0f;

    public float maxY = -65;
    public float minY = 50;
    Vector3 moveDirection = Vector3.zero;

    [HideInInspector]
    public bool canMove = true;
    // Start is called before the first frame update
    void Start()
    {
        arcade.SetActive(false);
        toystore.SetActive(false);
        playarea.SetActive(false);
        dollstore.SetActive(false);
        construction.SetActive(false);
        characterController = GetComponent<CharacterController>();

        //Lock cursor
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        //Press shift to run
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * -Input.GetAxis("Vertical") : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * -Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);
        
        if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
        {
            moveDirection.y = jumpSpeed;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }

        if(!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        characterController.Move(moveDirection * Time.deltaTime);

        yaw += rotMult * Input.GetAxis("Mouse X");
        pitch -= rotMult * -Input.GetAxis("Mouse Y");
        pitch = Mathf.Clamp(pitch, maxY, minY);

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
    }
}
