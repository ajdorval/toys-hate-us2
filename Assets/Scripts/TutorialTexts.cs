using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialTexts : MonoBehaviour
{
    public Image Text1;
    public Image Text2;
    public Image Text3;
    public Image Text4;
    public Image Text5;
    public Image Text6;
    public Text ClickProceed;
    public Image background;
    public GameObject Instructions;
    public GameObject character;
    public GameObject tutorialGroup;
    public int counter;

    // Start is called before the first frame update
    void Start()
    {
        Text1.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Text1.gameObject.SetActive(true);
        ClickProceed.gameObject.SetActive(true);
        Text2.gameObject.SetActive(false);
        Text3.gameObject.SetActive(false);
        Text4.gameObject.SetActive(false);
        Text5.gameObject.SetActive(false);
        Text6.gameObject.SetActive(false);
        background.gameObject.SetActive(true);
        Instructions.gameObject.SetActive(false);
        character.GetComponent<CharacterMovement>().canMove = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            counter++;
        }

        if (counter == 1)
        {
            Text1.gameObject.SetActive(false);
            Text2.gameObject.SetActive(true);
        }
        if (counter == 2)
        {
            Text2.gameObject.SetActive(false);
            Text3.gameObject.SetActive(true);
        }
        if (counter == 3)
        {
            Text3.gameObject.SetActive(false);
            Text4.gameObject.SetActive(true);
        }
        if (counter == 4)
        {
            Text4.gameObject.SetActive(false);
            Text5.gameObject.SetActive(true);
        }
        if (counter == 5)
        {
            Text5.gameObject.SetActive(false);
            Text6.gameObject.SetActive(true);
        }
        if (counter >= 6)
        {
            Text6.gameObject.SetActive(false);
            ClickProceed.gameObject.SetActive(false);
            Instructions.gameObject.SetActive(true);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Confined;
            character.GetComponent<CharacterMovement>().canMove = true;
            background.gameObject.SetActive(false);
        }
    }
    public IEnumerator instructionTutorial()
    {
        yield return new WaitForSeconds(10);
        character.GetComponent<CharacterMovement>().canMove = true;
        tutorialGroup.gameObject.SetActive(false);
        Cursor.visible = false;
    }

}
