using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClueStuff : MonoBehaviour
{
    public GameObject arcade;
    public GameObject playarea;
    public GameObject toyStore;
    public GameObject dollStore;
    public GameObject construction;
    public GameObject smartphoneClue;
    public GameObject backpackClue;
    public GameObject jacketClue;
    public GameObject duckyClue;
    public GameObject spinnerClue;
    public GameObject outOfBounds;
    public Text GoalText;

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("On Trigger Enter");
        if (col.GetComponent<Collider>().tag == "Clue")
        {
            Debug.Log("Clue Collision!");
            if (col.GetComponent<Collider>().name == "smartphone")
            {
                smartphoneClue.gameObject.SetActive(true);
                StartCoroutine("clueDisappear");
                GoalText.text = "Check the play area upstairs.";
                Destroy(col.gameObject);
            }
            if (col.GetComponent<Collider>().name == "backpack")
            {
                backpackClue.gameObject.SetActive(true);
                StartCoroutine("clueDisappear");
                GoalText.text = "Maybe the dollstore or the arcade?";
                Destroy(col.gameObject);
            }
            if (col.GetComponent<Collider>().name == "jacket2.0")
            {
                jacketClue.gameObject.SetActive(true);
                StartCoroutine("clueDisappear");
                GoalText.text = "Check the Arcade.";
                Destroy(col.gameObject);
            }
            Debug.Log(col.GetComponent<Collider>().name);
        }

        if (col.GetComponent<Collider>().tag == "Duck")
        {
            Debug.Log("duck");
            duckyClue.gameObject.SetActive(true);
            StartCoroutine("clueDisappear");
            Destroy(col.gameObject);
        }

        if (col.GetComponent<Collider>().tag == "Spinner")
        {
            spinnerClue.gameObject.SetActive(true);
            StartCoroutine("clueDisappear");
            Destroy(col.gameObject);
        }
        //adding but for checkpoints to reappear when people pass by certain places
        if (col.GetComponent<Collider>().tag == "Arcade")
        {
            Debug.Log("Arcade Collision!");
            if (arcade.activeSelf)
            {
                Debug.Log("Arcade no more!");
                arcade.SetActive(false);
                Destroy(col.gameObject);
            }
            else
            {
                Debug.Log("Arcade again!");
                arcade.SetActive(true);
                Destroy(col.gameObject);
            }
        }

        if (col.GetComponent<Collider>().tag == "Construction")
        {
            Debug.Log("Construction Collision!");
            if (construction.activeSelf)
            {
                Debug.Log("Construction no more!");
                construction.SetActive(false);
                Destroy(col.gameObject);
            }
            else
            {
                Debug.Log("Construction again!");
                construction.SetActive(true);
                Destroy(col.gameObject);
            }
        }

        if (col.GetComponent<Collider>().tag == "DollStore")
        {
            Debug.Log("DollStore Collision!");
            if (dollStore.activeSelf)
            {
                Debug.Log("dollstore no more!");
                dollStore.SetActive(false);
                GoalText.text = "Check the toystore.";
                Destroy(col.gameObject);
            }
            else
            {
                Debug.Log("dollstore again!");
                dollStore.SetActive(true);
                GoalText.text = "Check the dollstore.";
                Destroy(col.gameObject);
            }
        }
        if (col.GetComponent<Collider>().tag == "PlayArea")
        {
            Debug.Log("playArea Collision!");
            if (playarea.activeSelf)
            {
                Debug.Log("playarea no more!");
                playarea.SetActive(false);
                GoalText.text = "Maybe the dollstore or the arcade?";
                Destroy(col.gameObject);
            }
            else
            {
                Debug.Log("playarea again!");
                playarea.SetActive(true);
                Destroy(col.gameObject);
            }
        }
        if (col.GetComponent<Collider>().tag == "ToyStore")
        {
            Debug.Log("toystore Collision!");
            if (toyStore.activeSelf)
            {
                Debug.Log("toystore no more!");
                toyStore.SetActive(false);
                Destroy(col.gameObject);
            }
            else
            {
                Debug.Log("toystore again!");
                toyStore.SetActive(true);
                Destroy(col.gameObject);
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "border")
        {
            Debug.Log("border Collision!");
            outOfBounds.gameObject.SetActive(true);
            StartCoroutine("clueDisappear");
        }
    }

    public IEnumerator clueDisappear()
    {
        yield return new WaitForSeconds(6);
        smartphoneClue.SetActive(false);
        backpackClue.SetActive(false);
        jacketClue.SetActive(false);
        duckyClue.SetActive(false);
        spinnerClue.SetActive(false);
        outOfBounds.SetActive(false);
    }
}