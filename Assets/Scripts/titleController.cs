using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class titleController : MonoBehaviour
{
    public CanvasGroup controls;
    public CanvasGroup settings;
    // Start is called before the first frame update
    public void ChangeSceneByName(string name)
    {
        if (name != null)
        {
            SceneManager.LoadScene(name);
        }
    }
    public void controlOn ()
    {
        controls.gameObject.SetActive(true);
    }
    public void controlOff()
    {
        controls.gameObject.SetActive(false);
    }
    public void settingsOn()
    {
        settings.gameObject.SetActive(true);
    }
    public void settingsOff()
    {
        settings.gameObject.SetActive(false);
    }
}
