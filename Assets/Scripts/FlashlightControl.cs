using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashlightControl : MonoBehaviour
{
    public GameObject FlashlightLight;
    public Slider FlashlightInside;
    public Text PercentNumber;
    public Text BatteryNumber;
    private bool lightOn;
    private float maxPercent;
    private float batteryPercent;
    private float batteryInt;
    private bool lock1 = false;
    // Start is called before the first frame update
    void Start()
    {
        lightOn = false;
        FlashlightLight.gameObject.SetActive(value: lightOn);
        batteryPercent = 100;
        maxPercent = 100;
        FlashlightInside.maxValue = 100;
        FlashlightInside.value = 100;
        PercentNumber.text = "100";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            lightOn = !lightOn;
            FlashlightLight.gameObject.SetActive(value: lightOn);
            if (lock1 == true)
            {
                lock1 = false;
            }
        }
        if (lightOn == true && batteryPercent > 0 && lock1 == false)
        {
            lock1 = true;
            StartCoroutine("flashlightBattery");
        }
        else if (batteryPercent <= 0)
        {
            lightOn = false;
            FlashlightLight.gameObject.SetActive(value: lightOn);
            lock1 = false;
        }

        if (Input.GetKeyUp(KeyCode.R) && batteryInt > 0)
        {
            if ((batteryPercent + 30) < maxPercent)
            {
                batteryPercent += 30;
                FlashlightInside.value = batteryPercent;
                PercentNumber.text = batteryPercent.ToString();
                batteryInt--;
                BatteryNumber.text = batteryInt.ToString();
            }
            if ((batteryPercent + 30) > maxPercent)
            {
                batteryPercent = 100;
                FlashlightInside.value = batteryPercent;
                PercentNumber.text = batteryPercent.ToString();
                batteryInt--;
                BatteryNumber.text = batteryInt.ToString();
            }
        }
    }

    public void batteryIncrease(Collider col)
    {
        Debug.Log("battery!");
        batteryInt++;
        BatteryNumber.text = batteryInt.ToString();
        Destroy(col.gameObject);
    }

    public void OnCollisionEnter(Collision col)
    {
        Debug.Log("collision");
        if (col.collider.tag == "Battery") //if colliding with battery, add a battery
        {
            Debug.Log("battery!");
            batteryInt++;
            BatteryNumber.text = batteryInt.ToString();
            Destroy(col.gameObject);
        }
    }

    public IEnumerator flashlightBattery()
    {
        while (lightOn == true)
        {
            batteryPercent--;
            FlashlightInside.value = batteryPercent;
            PercentNumber.text = batteryPercent.ToString();
            Debug.Log(batteryPercent);
            yield return new WaitForSeconds(1);

        }
    }
}
