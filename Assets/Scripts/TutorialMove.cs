using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialMove : MonoBehaviour
{
    public Text WASDText;
    public Text ShiftText;
    public Text SpaceText;
    public Text EText;
    public Text MouseText;

    // Start is called before the first frame update
    void Start()
    {
        //MouseText.gameObject.SetActive(true);
        //StartCoroutine("mouseUI");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            WASDText.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            ShiftText.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            SpaceText.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.E))
        {
            EText.gameObject.SetActive(false);
        }
    }
    public IEnumerator mouseUI()
    {
       // Debug.Log("mouse ui coroutine");
        yield return new WaitForSeconds(5);
      //  Debug.Log("waited 5 seconds");
        MouseText.gameObject.SetActive(false);
    }
}
