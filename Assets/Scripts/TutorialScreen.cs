using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialScreen : MonoBehaviour
{
    public Text tutorialText;
    public Image grayBackground;
    // Start is called before the first frame update
    void Start()
    {
        grayBackground.gameObject.SetActive(true);
        tutorialText.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    { //sets it so if the player moves in any way, the tutorial disappears
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Space)) {
            grayBackground.gameObject.SetActive(false);
            tutorialText.gameObject.SetActive(false);
        }
    }
}
