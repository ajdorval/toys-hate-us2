using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.InputSystem.Users;



public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    float currentVolume;

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("MusicVol", volume);
        currentVolume = volume;
    }
}
