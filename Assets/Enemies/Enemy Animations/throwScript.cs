using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class throwScript : MonoBehaviour
{
    Transform player;
    public Transform throwSpot;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Transform>();
    }

    void OnTriggerEnter()
    {
        Vector3 worldOfThrow = throwSpot.TransformPoint(Vector3.zero);
        player.position = worldOfThrow;
    }
    // Update is called once per frame
    void Update()
    {
        //maybe need something?
    }
}
