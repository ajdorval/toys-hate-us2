using UnityEngine;
using System;
using System.Collections;
using UnityEngine.AI;

public class Playerfollow : MonoBehaviour
{
    public GameObject target;
    Animator anim;
    NavMeshAgent agent;
    public float patrolRadius;
    Vector3 playerPos;
    Boolean move;
    MeshRenderer render;
    
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
        anim.enabled = false;
        render = GetComponent<MeshRenderer>();
    }

    void OnBecomeInvisible()
    {
        Console.WriteLine("moving?");
        render.enabled = false; //makes thing invisible
    }

    void OnBecomeVisible()
    {
        Console.WriteLine("not moving?");
        render.enabled = true;
    }

    float getDistance()
    {
        float xDist = (agent.transform.position.x - playerPos.x);
        float zDist = (agent.transform.position.z - playerPos.z);
        float xSquare = Convert.ToSingle(Math.Pow(xDist, 2));
        float zSquare = Convert.ToSingle(Math.Pow(zDist, 2));
        return Convert.ToSingle(Math.Sqrt(xSquare + zSquare));
    }

    void LateUpdate()
    {
        playerPos = target.transform.position;
        float distance = this.getDistance();
        if (distance <= patrolRadius)
        {
            anim.enabled = true;
            agent.destination = playerPos;
            anim.SetTrigger("Moving");
            if (agent.remainingDistance < 5)
            {
                anim.SetTrigger("OnPlayer");
            }
        }
        else
        {
           // print("Should not move because out of range");
            agent.destination = agent.transform.localPosition;
            anim.SetTrigger("NoPlayer");
            anim.enabled = false;
        }
    }
}
